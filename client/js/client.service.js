(
    () => {
        //NOTE: The single-quote for custom service and double-quotes for Angular service do not apply here
        //IMPORTS INTO REGVM
        angular.module("RegApp").service("RegSvc",['$http',RegSvc]);

        //NOTE: Used for Angular dependencies, express custom service in string within single quotes only and Angular service $ with double service
        //EXPORT FROM REGVM
        RegSvc.$inject = ["RegVM"];
        
        function RegSvc($http) {
            const svc = this;
            console.log(RegSvc);
            svc.onSubmit = (selected) => {
                console.log(selected);
                console.log(selected.email);
                console.log(selected.password);
                console.log(selected.confirmPassword);
                console.log(selected.fullName);
                console.log(selected.gender);
                console.log(selected.dob);
                console.log(selected.postalAddress);
                console.log(selected.nationality);
                console.log(selected.contactNo);
                $http.post(
                    "/api/submit",
                    selected
                ).then(
                    (res) => {
                        console.log(res);
                    }
                ).catch(
                    (error) => {
                        console.log(error);
                    }
                );
            }
        }
    }
)();