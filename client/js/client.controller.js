/**
 * Client_side client.controller.js
 * Angular js
 */
"use strict";
(
    () => {
      "use strict";
      //NOTE: The single-quote for custom service and double-quotes for Angular service do not apply here
      //IMPORTS INTO REGVM
      angular
        .module("RegApp")
        .controller("RegVM", [RegVM])
        .controller("RegisteredVM", RegisteredVM);

      //NOTE: Used for Angular dependencies, express custom service in string within single quotes only and Angular service $ with double service
      //EXPORT FROM REGVM
      RegVM.$inject = ["RegSvc", "$log"];
      RegisteredVM.$inject = ["RegSvc", "$log"];

      function RegVM(RegSvc, $log) {
        const reg = this;
        reg.SELECT_DEFAULT = { name: "Please select", value: "0" };
        reg.selected = { email: "", nationality: "" };
        reg.nationalities = [{ name: "Singaporean", value: "1" }, { name: "Indonesian", value: "2" }, { name: "Thai", value: "3" }, { name: "Malaysian", value: "4" }];

        reg.onSubmit = () => {
          console.log(RegSvc);
          RegSvc.onSubmit(reg.selected)
            .then(result => {
              reg.selected = result.data;
              conole.log($log);
            })
            .catch(error => {
              console.log(error);
            });
        };

        reg.onlyFemales = () => {
          console.log("Only females are selected.");
          return reg.selected.gender == "Female";
        };

        //On load invokes the Registration form
        (() => {
          reg.selected.nationality = "0";
        })();
      };

      function RegisteredVM(RegSvc, $log) {
        const regd = this;
        const reg = RegVM;
        regd.fullName = reg.fullName;
      };
    }
)();