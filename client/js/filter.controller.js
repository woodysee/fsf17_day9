/**
 * Client_side filter.controller.js
 * Angular js
 */
"use strict";
(
    () => {
        //NOTE: The single-quote for custom service and double-quotes for Angular service do not apply here
        //IMPORTS INTO REGVM
        "use strict";
        angular.module("RegApp").controller("FilterVM",FilterVM);

        //NOTE: Used for Angular dependencies, express custom service in string within single quotes only and Angular service $ with double service
        //EXPORT FROM REGVM
        FilterVM.$inject = [];

        function FilterVM() {
            const vm = this;
            vm.search = "";
            vm.orderBy = "id"; //default sort by id
            vm.reverse = true; //in-built
            const today = new Date();

            vm.records = [
                {id: 1, name: "Andy Lau", age: 20},
                {id: 2, name: "Ben Lau", age: 20},
                {id: 32, name: "Charmaine Lau", age: 20},
                {id: 4, name: "Doris Lau", age: 20},
                {id: 5, name: "Edgar Lau", age: 20},
                {id: 6, name: "Frederick Lau", age: 20},
                {id: 7, name: "George Lau", age: 20},
                {id: 8, name: "Heather Lau", age: 20},
                {id: 9, name: "Ivan Lau", age: 20}
            ];

            vm.sortBy = (sort) => {
                vm.reverse = (vm.orderBy === sort) ? !vm.reverse : false;
                vm.sortBy = sort;
            };
        }
    }
)();