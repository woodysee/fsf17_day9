/**
 * Client_side animate.controller.js
 * Angular js
 */
"use strict";
(() => {
  //NOTE: The single-quote for custom service and double-quotes for Angular service do not apply here
  //IMPORTS INTO REGVM
  "use strict";
  angular.module("RegApp").controller("AnimateVM", AnimateVM);

  //NOTE: Used for Angular dependencies, express custom service in string within single quotes only and Angular service $ with double service
  //EXPORT FROM REGVM
  AnimateVM.$inject = [];

  function AnimateVM() {
    const animate = this;
    animate.class = {
        box: true,
        circle: true,
        triangle: false
    };
  };
})();
