/**
 * Client_side client.module.js
 * 
 */

"use strict";
(
    () => {
        console.log("Loading client with Angular 1.x below:")
        console.log(angular);
        angular.module("RegApp", []);
    }

)();