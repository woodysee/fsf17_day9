# Registration Form #

Angular 1.x Learning Journey with a Registration Form

## Creating a service ##

```
$http.get(
    '/someURL',
    funfunction()
).then(
    successCallback,
    errorCallback
)
```

```
$http({
    method: 'GET',
    url: '/someUrl'
}).then(
    (response) => {
});
```

In `/client.controller.js`,
```
Controller.$inject = ["$http","$log","$window"]

Controller = (a,b,c) => {
    a.post(...),
};

```
The order of arguments have to correspond with the dependency injection order above, no matter what you call your arguments within the controller definition scope.

e.g. `$http = a`,`$log = b`, `$window = c`, `$http != b`...

## Asynchronous execution in action ##
```
generic = (callback) => {
    $http.get("/api/users").then(
        (RESULT) => {
            callback(RESULT);   
        }
    ).catch(
        (error) => {
            console.log(error);
        }
    );
};

generic(
    (RESULT) => {
        console.log(RESULT);
    }
);
```

## Setting up a script ##

1. Touch `/client.service.js`.
2. In `/index.html` view, the script tag for service should be imported before the script tag for controller.
3. Implement the `$service`
4. Remember that single quotes only for your custom services.